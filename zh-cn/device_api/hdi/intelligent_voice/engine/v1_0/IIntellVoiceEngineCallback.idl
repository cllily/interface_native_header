/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceEngine
 * @{
 *
 * @brief IntelligentVoiceEngine模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上统一接口获取如下能力：创建销毁唤醒算法引擎、启动停止唤醒算法引擎、写语音数据、读文件、回调函数注册等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IIntellVoiceEngineCallback.idl
 *
 * @brief IntelligentVoiceEngine模块智能语音引擎回调接口，用于通知上层服务事件信息。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.engine.v1_0
 *
 * 引用：ohos.hdi.intelligent_voice.engine.v1_0.IntellVoiceEngineTypes
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.intelligent_voice.engine.v1_0;

import ohos.hdi.intelligent_voice.engine.v1_0.IntellVoiceEngineTypes;

 /**
 * @brief IntelligentVoiceEngine模块向上层服务提供了智能语音引擎回调接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上智能语音引擎回调接口实现底层事件上报的功能。
 *
 * @since 4.0
 * @version 1.0
 */
[callback] interface IIntellVoiceEngineCallback {
    /**
     * @brief 引擎回调函数。
     *
     * @param event 智能语音引擎回调事件信息，具体参考{@link IntellVoiceEngineCallBackEvent}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    OnIntellVoiceHdiEvent([in] struct IntellVoiceEngineCallBackEvent event);
}
/** @} */