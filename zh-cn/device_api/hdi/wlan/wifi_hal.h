/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WLAN
 * @{
 *
 * @brief WLAN模块向上层WLAN服务提供了统一接口。
 *
 * HDI层开发人员可根据WLAN模块提供的向上统一接口获取如下能力：建立/关闭WLAN热点，扫描，关联WLAN热点，WLAN平台芯片管理，网络数据缓冲的申请、释放、移动等操作，网络设备管理，电源管理等。
 *
 *
 * @since 1.0
 * @version 1.0
 */

/**
 * @file wifi_hal.h
 *
 * @brief 提供给WLAN服务的WLAN基本能力接口。
 *
 * @since 1.0
 * @version 1.0
 */

#ifndef WIFI_HAL_H
#define WIFI_HAL_H

#include "wifi_hal_ap_feature.h"
#include "wifi_hal_sta_feature.h"

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

/**
 * @brief 定义IWiFi回调函数的原型，监听异步事件。
 *
 * @param event 输入参数，回调传入的事件类型标识。
 * @param data 输入参数，回调传入的数据。
 * @param ifName 输入参数，网卡名称。
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
typedef int32_t (*CallbackFunc)(uint32_t event, void *data, const char *ifName);

/**
 * @brief HAL对WLAN服务提供的基本能力。
 *
 * 用于创建HAL与驱动的通道，创建/获取/销毁WLAN特性等。
 *
 *
 * @since 1.0
 * @version 1.0
 */
struct IWiFi {
    /**
     * @brief 创建HAL和驱动之间的通道及获取驱动网卡信息，该函数调用在创建IWiFi实体后进行。
     *
     * @param iwifi 输入参数，IWiFi对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*start)(struct IWiFi *iwifi);

    /**
     * @brief 销毁HAL和驱动之间的通道，该函数调用在销毁IWiFi实体前进行。
     *
     * @param iwifi 输入参数，IWiFi对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*stop)(struct IWiFi *iwifi);

    /**
     * @brief 获取该设备支持的WLAN特性（不考虑当前的使用状态）。
     *
     *
     * @param supType 输出参数，保存当前设备支持的特性。
     * @param size 输入参数，supType数组的长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getSupportFeature)(uint8_t *supType, uint32_t size);

    /**
     * @brief 获取多网卡共存情况。
     *
     *
     * @param combo 输出参数，基于芯片的能力保存当前所有支持的多网卡共存情况（比如支持AP，STA，P2P等不同组合的共存）。
     * @param size 输入参数，combo数组的长度。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getSupportCombo)(uint64_t *combo, uint32_t size);

    /**
     * @brief 根据输入类型创建对应的特性{@link IWiFiBaseFeature}。
     *
     * @param type 输入参数，创建的feature类型。
     * @param ifeature 输出参数，获取创建的feature对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*createFeature)(int32_t type, struct IWiFiBaseFeature **ifeature);

    /**
     * @brief 通过网络接口名字获取对应的特性。
     *
     * @param ifName 输入参数，网卡名称。
     * @param ifeature 输出参数，获取该网络接口名字的feature对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getFeatureByIfName)(const char *ifName, struct IWiFiBaseFeature **ifeature);

    /**
     * @brief 注册IWiFi的回调函数，监听异步事件。
     *
     * @param cbFunc 输入参数，注册的回调函数。
     * @param ifName 输入参数，网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*registerEventCallback)(CallbackFunc cbFunc, const char *ifName);

    /**
     * @brief 去注册IWiFi的回调函数。
    
     * @param cbFunc 输入参数，去注册的回调函数。
     * @param ifName 输入参数，网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*unregisterEventCallback)(CallbackFunc cbFunc, const char *ifName);

    /**
     * @brief 根据输入类型销毁对应的特性{@link IWiFiBaseFeature}。
     *
     * @param ifeature 输入参数，销毁的feature对象。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*destroyFeature)(struct IWiFiBaseFeature *ifeature);

    /**
     * @brief 重置具有指定芯片ID的WLAN驱动程序。
     *
     * @param chipId 输入参数，需要进行重置驱动的对应芯片ID。
     * @param ifName 输入参数，网卡名称。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*resetDriver)(const uint8_t chipId, const char *ifName);

    /**
     * @brief 获取网络设备信息（设备索引、网卡名字、MAC等信息）。
     *
     * @param netDeviceInfoResult 输出参数，得到的网络设备信息。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getNetDevInfo)(struct NetDeviceInfoResult *netDeviceInfoResult);

    /**
     * @brief 获取正在使用的功率模式。
     *
     * @param ifName 输入参数，网卡名称。
     * @param mode 输出参数，功率模式，包括睡眠模式（待机状态运行）、一般模式（正常额定功率运行）、穿墙模式（最大功率运行，提高信号强度和覆盖面积）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*getPowerMode)(const char *ifName, uint8_t *mode);

    /**
     * @brief 设置功率模式
     *
     * @param ifName 输入参数，网卡名称。
     * @param mode 输入参数，功率模式,包括睡眠模式（待机状态运行）、一般模式（正常额定功率运行）、穿墙模式（最大功率运行，提高信号强度和覆盖面积）。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 1.0
     * @version 1.0
     */
    int32_t (*setPowerMode)(const char *ifName, uint8_t mode);
};

/**
 * @brief 创建IWiFi结构体，挂接{@link IWiFi}中能力接口。
 *
 * @param wifiInstance HAL服务对象{@link IWiFi}
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t WifiConstruct(struct IWiFi **wifiInstance);

/**
 * @brief 销毁IWiFi结构体并释放相关资源。
 *
 * @param wifiInstance HAL服务对象{@link IWiFi}
 *
 * @return 如果操作成功，则返回0。
 * @return 如果操作失败，则返回负值。
 *
 * @since 1.0
 * @version 1.0
 */
int32_t WifiDestruct(struct IWiFi **wifiInstance);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif
/** @} */
