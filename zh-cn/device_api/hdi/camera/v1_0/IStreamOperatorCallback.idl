/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IStreamOperatorCallback.idl
 *
 * @brief {@link IStreamOperator}相关的回调，这些回调均由调用者实现。
 *
 * 模块包路径：ohos.hdi.camera.v1_0
 *
 * 引用：ohos.hdi.camera.v1_0.Types
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief 定义Camera设备流回调操作。
 *
 * 对Camera设备执行流回调的抓捕，结束，错误捕获和帧捕获等操作。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IStreamOperatorCallback {
     /**
     * @brief 捕获开始回调，在捕获开始时调用。
     *
     * @param captureId 用于标识回调对应的捕获请求。
     * @param streamIds 回调对应的流集合。
     *
     * @see OnCaptureEnded
     *
     * @since 3.2
     * @version 1.0
     */
    OnCaptureStarted([in] int captureId, [in] int[] streamIds);

     /**
     * @brief 捕获结束回调，在捕获结束时调用。
     *
     * @param captureId 用于标识回调对应的捕获请求。
     * @param infos 捕获结束相关信息，具体结束相关信息查看{@link CaptureEndedInfo}。
     *
     * @see OnCaptureStarted
     *
     * @since 3.2
     * @version 1.0
     */
    OnCaptureEnded([in] int captureId, [in] struct CaptureEndedInfo[] infos);

     /**
     * @brief 捕获错误回调，在捕获过程中发生错误时调用。
     *
     * @param captureId 用于标识回调对应的捕获请求。
     * @param infos 捕获错误信息列表，具体错误信息查看{@link CaptureErrorInfo}。
     *
     * @since 3.2
     * @version 1.0
     */
    OnCaptureError([in] int captureId, [in] struct CaptureErrorInfo[] infos);

     /**
     * @brief 帧捕获回调。
     *
     * 通过{@link Capture}的输入参数{@link CaptureInfo}的enableShutterCallback_使能该回调，
     * 使能后每次捕获均会触发此回调。
     *
     * @param captureId 用于标识回调对应的捕获请求。
     * @param streamIds 回调对应的流集合。
     * @param timestamp 该接口被调用时的时间戳。
     *
     * @see Capture
     *
     * @since 3.2
     * @version 1.0
     */
    OnFrameShutter([in] int captureId, [in] int[] streamIds, [in] unsigned long timestamp);
}
/** @} */
