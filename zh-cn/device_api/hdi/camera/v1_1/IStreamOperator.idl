/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @file IStreamOperator.idl
 *
 * @brief 流的操作接口。
 *
 * 模块包路径：ohos.hdi.camera.v1_1
 *
 * 引用：
 * - ohos.hdi.camera.v1_0.IStreamOperator
 * - ohos.hdi.camera.v1_1.Types
 *
 * @since 4.0
 * @version 1.1
 */


package ohos.hdi.camera.v1_1;

import ohos.hdi.camera.v1_0.IStreamOperator;
import ohos.hdi.camera.v1_1.Types;

/**
 * @brief 定义Camera设备流操作。
 *
 * 对Camera设备执行流的创建、配置与添加参数、属性获取、句柄绑定与解除、图像捕获与取消、流的转换以及流释放操作。
 *
 * 流是指从底层设备输出，经本模块内部各环节处理，最终传递到上层服务或者应用的一组数据序列。
 * 本模块支持的流的类型有预览流，录像流，拍照流等，更多类型可查看{@link StreamIntent}。
 *
 * @since 4.0
 * @version 1.1
 */
interface IStreamOperator extends ohos.hdi.camera.v1_0.IStreamOperator {
    /**
     * @brief 查询是否支持添加参数对应的流。
     *
     * 此函数接口根据输入的运行模式和配置信息以及当前模块中正在运行的流，查询是否支持动态添加流。
     * - 如果本模块支持在不停止其他流的情况下添加新流，或者即使停止其他流但上层服务或应用不感知，
     *   则通过type参数返回DYNAMIC_SUPPORTED，上层服务或应用可以直接添加新流；
     * - 如果本模块支持添加新流但需要上层服务或应用先停止所有流的捕获，则通过type参数返回RE_CONFIGURED_REQUIRED;
     * - 如果不支持添加输入的新流，则返回NOT_SUPPORTED。
     *
     * 此函数需要在调用{@link CreateStreams}创建流之前调用。
     *
     * @param mode 流的使用模式，支持的模式参考{@link OperationMode}。
     * @param modeSetting 流的配置，包括帧率，3A等配置信息。3A：自动曝光 (AE)、自动聚焦 (AF)、自动白平衡 (AWB)
     * @param infos 流的配置信息，具体参考{@link StreamInfo}。
     * @param type 对动态配置流的支持类型，支持类型定义在{@link StreamSupportType}。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.0
     * @version 1.1
     */
    IsStreamsSupported_V1_1([in] enum OperationMode_V1_1 mode, [in] unsigned char[] modeSetting,
        [in] struct StreamInfo_V1_1[] infos, [out] enum StreamSupportType type);

    /**
     * @brief 创建流。
     *
     * 此函数接口依据输入的流信息创建流，调用该接口之前需先通过{@link IsStreamsSupported}查询HAL是否支持要创建的流。
     *
     * @param streamInfos 流信息列表，流信息定义在{@link StreamInfo}。输入的流信息可能会被修改，需通过
     * {@link GetStreamAttributes}获取最新的流属性。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.0
     * @version 1.1
     */
    CreateStreams_V1_1([in] struct StreamInfo_V1_1[] streamInfos);

    /**
     * @brief 配置流。
     *
     * 本接口需在调用{@link CreateStreams}创建流之后调用。
     *
     * @param mode 流运行的模式，支持的模式定义在{@link OperationMode}。
     * @param modeSetting 流的配置参数，包括帧率，ZOOM等信息。ZOOM：变焦。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 4.0
     * @version 1.1
     */
    CommitStreams_V1_1([in] enum OperationMode_V1_1 mode, [in] unsigned char[] modeSetting);
}
/** @} */