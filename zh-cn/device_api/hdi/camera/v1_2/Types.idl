/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file Types.idl
 *
 * @brief Camera模块HDI接口使用的数据类型。
 *
 * 模块包路径：ohos.hdi.camera.v1_2
 *
 * 引用：ohos.hdi.camera.v1_1.Types
 *
 * @since 4.1
 * @version 1.2
 */

package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_1.Types;

sequenceable ohos.hdi.camera.v1_0.BufferHandleSequenceable;
sequenceable ohos.hdi.camera.v1_0.MapDataSequenceable;

/**
 * @brief HDI接口的返回值。
 *
 * @since 4.1
 * @version 1.2
 */
enum CamRetCode {
    /**
     * 调用成功。
     */
    NO_ERROR = 0,

    /**
     * 设备当前忙。
     */
    CAMERA_BUSY = -1,

    /**
     * 资源不足。
     */
    INSUFFICIENT_RESOURCES = -2,

    /**
     * 参数错误。
     */
    INVALID_ARGUMENT = -3,

    /**
     * 不支持当前调用方法。
     */
    METHOD_NOT_SUPPORTED = -4,

    /**
     * Camera设备已经关闭。
     */
    CAMERA_CLOSED = -5,

    /**
     * 驱动层发生严重错误。
     */
    DEVICE_ERROR = -6,

    /**
     * 无权限访问设备。
     */
    NO_PERMISSION = -7,

    /**
     * 设备冲突。
     */
    DEVICE_CONFLICT = -8
};

/**
 * @brief 扩展流信息的类型。
 *
 * @since 4.1
 * @version 1.2
 */
enum ExtendedStreamInfoType_V1_2 {
    /**
     * 快速缩略图的扩展流信息。
     */
    EXTENDED_STREAM_INFO_QUICK_THUMBNAIL = 0,

    /**
     * sketch的扩展流信息。
     */
    EXTENDED_STREAM_INFO_SKETCH = 1,
};

/**
 * @brief 流使用模式。
 *
 * @since 4.1
 * @version 1.2
 */
enum OperationMode_V1_2 {
    /**
     * 普通模式，支持照片和视频场景。
     */
    NORMAL = 0,

    /**
     * 拍摄模式，专用于照片场景。
     * 如果实现了此模式，则不应再实现 NORMAL 模式。
     */
    CAPTURE = 1,

    /**
     * 视频模式，专用于视频秒控。
     * 如果实现了此模式，则不应再实现 NORMAL 模式。
     */
    VIDEO = 2,

    /**
     * 人像模式，专用于人像照片拍摄。
     */
    PORTRAIT = 3,

    /**
     * 夜间模式，专用于夜间拍摄场景。
     */
    NIGHT = 4,

    /**
     * 专业模式，专用于专业拍照场景。
     */
    PROFESSIONAL = 5,

    /**
     * 慢动作模式，专用于捕捉慢动作。
     */
    SLOW_MOTION = 6,

    /**
     * 扫描模式，专用于扫码。
     */
    SCAN_CODE = 7,

    /**
     * 微距模式，专用于微距拍照。
     */
    CAPTURE_MACRO = 8,

    /**
     * 微距模式，专用于微距录像。
     */
    VIDEO_MACRO = 9,

    /**
     * 超级防抖模式，专用于使用超级防抖模式。
     */
    SUPER_STAB = 10,
};

/**
 * @brief 延迟拍照的类型。
 *
 * @since 4.1
 * @version 1.2
 */
enum DeferredDeliveryImageType {
    /**
     * 不支持延迟拍照。
     */
    NONE = 0,

    /**
     * 支持静止图像。
     */
    STILL_IMAGE = 1,

    /**
     * 支持动态图像。
     */
    MOVING_IMAGE = 2,
};

/**
 * @brief 会话状态的类型。
 *
 * @since 4.1
 * @version 1.2
 */
enum SessionStatus {
    /**
     * 会话已准备就绪。
     */
    SESSION_STATUS_READY = 0,

    /**
     * 会话已准备就绪，但已达到存储限制。
     */
    SESSION_STATUS_READY_SPACE_LIMIT_REACHED = 1,

    /**
     * 会话暂时未就绪。
     */
    SESSSON_STATUS_NOT_READY_TEMPORARILY = 2,

    /**
     * 由于过热，会话未就绪。
     */
    SESSION_STATUS_NOT_READY_OVERHEAT = 3,

    /**
     * 由于被抢占，会话未就绪。
     */
    SESSION_STATUS_NOT_READY_PREEMPTED = 4,
};

/**
 * @brief 错误代码的类型。
 *
 * @since 4.1
 * @version 1.2
 */
enum ErrorCode {
    /**
     * 超时。
     */
    TIMEOUT = 0,

    /**
     * 错误。
     */
    ERROR = 1,

    /**
     * 忙碌。
     */
    BUSY = 3,

    /**
     * 高温。
     */
    HIGH_TEMPERATURE = 4,

    /**
     * 中止。
     */
    ABORT = 5,
};

/**
 * @brief 执行模式的类型。
 *
 * @since 4.1
 * @version 1.2
 */
enum ExecutionMode {
    /**
     * 高性能模式。
     */
    HIGH_PREFORMANCE = 0,

    /**
     * 平衡模式。
     */
    BALANCED = 1,

    /**
     * 低功耗模式。
     */
    LOW_POWER = 2,
};

/**
 * @brief 定义ImageBufferInfo，它由{@link IImageProcessCallback::OnProcessDone}使用。
 *
 * @since 4.1
 * @version 1.2
 */
struct ImageBufferInfo {
    /**
     * metadata是否有效。
     */
    boolean isMetaDataValid;

    /**
     * metadata的数据。
     */
    MapDataSequenceable metadata;

    /**
     * ImageHandle的数据。
     */
    BufferHandleSequenceable imageHandle;

    /**
     * gainMapHandle是否有效。
     */
    boolean isGainMapValid;

    /**
     * gainMapHandle的数据。
     */
    BufferHandleSequenceable gainMapHandle;

    /**
     * depthMapHandle是否有效。
     */
    boolean isDepthMapValid;

    /**
     * depthMapHandle的数据。
     */
    BufferHandleSequenceable depthMapHandle;
};

/**
 * @brief 定义CaptureStartedInfo，该信息由{@link IStreamOperatorCallback::OnCaptureStartedV1_2}使用。
 *
 * @since 4.1
 * @version 1.2
 */
struct CaptureStartedInfo {
    /**
     * 流的ID，用于在设备内唯一标识一条流。
     */
    int streamId_;

    /**
     * 曝光时间，单位为毫秒。
     */
    int exposureTime_;
};
/** @} */