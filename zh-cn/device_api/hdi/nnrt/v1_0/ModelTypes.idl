/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup NNRt
 * @{
 *
 * @brief Neural Network Runtime（NNRt, 神经网络运行时）是面向AI领域的跨芯片推理计算运行时，作为中间桥梁连通上层AI推理框架和底层加速芯片，实现AI模型的跨芯片推理计算。提供统一AI芯片驱动接口，实现AI芯片驱动接入OpenHarmony。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ModelTypes.idl
 *
 * @brief 该文件定义AI模型相关的结构体。
 *
 * 在{@link PrepareModel}阶段，需要解析Model并将其转换为用于推理的模型结构，在{@link Run}阶段则会执行模型推理。大致流程如下：
 * - 1. 编写{@link NodeAttrTypes.idl}文件中每一个算子的函数，并将函数与{@link NodeType}进行关联；
 * - 2. 遍历{@link Model}的subGraph参数，然后从子图的nodeIndecies中获得该子图包含的算子节点以及算子的输入输出张量和整个{@link Model}的输入输出张量。
 * - 3. 通过{@link Node}的nodeType参数找到算子函数，并构建用于运行时的模型结构。
 * - 4. 执行模型推理时，通过用户输入张量传递给模型并执行模型推理，最终输出模型推理的结果。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief NNRt模块的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.nnrt.v1_0;

import ohos.hdi.nnrt.v1_0.NnrtTypes;

/**
 * @brief 张量结构体。
 *
 * @since 3.2
 * @version 1.0
 */
struct Tensor {
    /** 张量名称。 */
    String name;
    /** 张量数据类型，详情请参考：{@link DataType}。 */
    enum DataType dataType;
    /** 张量维度数组。 */
    int[] dims;
    /** 张量数据的排列，详情请参考：{@link Format}。 */
    enum Format format;
    /** 进程通信时用于张量数据传输的结构体，详情请参考：{@link SharedBuffer}。 */
    struct SharedBuffer data;
    /**
     * 张量的量化参数数组。详情请参考：{@link QuantParam}。
     * 分为两种情况，如果长度为一，则所有轴公用一个量化；
     * 若长度不为一，则数组中的每一个量化参数和轴一一对应。
     */
    struct QuantParam[] quantParams;
};

/**
 * @brief 算子节点结构体。
 * 
 * nodeAttr参数是一段被序列化的数据，并调用OHOS的hdi的反序列化接口才能得到具体参数。
 *      大致流程如下：
 *      - 定义算子参数的结构体，OP op{}，其中OP可以被替换为{@link NodeAttrTypes.idl}的算子参数结构体，op是变量名;
 *      - 申明MessageParcle对象，用存储反序列化的数据，OHOS::MessageParcel data;
 *      - 将nodeAttr写入data中，data.WriteBuffer(nodeAttr.data(),nodeAttr.size());
 *      - 将data中的数据反序列化到op结构体中，(void)OPBlockUnmarshalling(data, op);<br>
 *      然后就可以在op中查看具体的算子的参数值。
 *
 *      例如：
 *      某一个算子的 nodeType为NODE_TYPE_FULL_CONNECTION，那么它所对应的算子参数结构体应该为{@link FullConnection}，
 *      则该算子具有四个参数：hasBias，useAxis，axis和activationType。<br>
 *      则按照如下流程调用：<br>
 *      FullConnection full_connection{};<br>
 *      OHOS::MessageParcel data;<br>
 *      data.WriteBuffer(nodeAttr.data(),nodeAttr.size());<br>
 *      (void)FullConnectionBlockUnmarshalling(data, full_connection);<br>
 *      至此FullConnection的四个参数就写入了full_connection中。
 * 
 * @since 3.2
 * @version 1.0
 */
struct Node {
    /** 算子节点的名称 。*/
    String name;
    /** 算子节点的类型，详情请参考：{@link NodeType}。 */
    enum NodeType nodeType;
    /**
     * 算子节点的参数对应的序列化数组。
     */
    byte[] nodeAttr;
    /** 算子节点的输入节点下标。 */
    unsigned int[] inputIndex;
    /** 算子节点的输出节点下标。 */
    unsigned int[] outputIndex;
    /** 算子节点的量化参数，详情请参考：{@link QuantType}。 */
    enum QuantType quantType;
};

/**
 * @brief 子图结构体。
 *
 * @since 3.2
 * @version 1.0
 */
struct SubGraph {
    /** 子图的名称。 */
    String name;
    /** 子图的输入子图在{@link Model}的subGraph数组中的下标。 */
    unsigned int[] inputIndices;
    /** 子图的输出子图在{@link Model}的subGraph数组中的下标。 */
    unsigned int[] outputIndices;
    /** 子图包含的算子节点在{@link Model}的nodes数组中的下标。 */
    unsigned int[] nodeIndices;
};

/**
 * @brief 模型结构体。
 * 
 * 该结构体中存储了模型推理时所有的信息，每一个Model的第0个子图都是主子图，一般情况下一个Model仅有一个subGraph（其他情况目前暂不支持）。
 * 
 * @since 3.2
 * @version 1.0
 */
struct Model {
    /** 模型名称。 */
    String name;
    /** 
     * 模型的输入张量在allTensors数组中的下标。
     */
    unsigned int[] inputIndex;
    /** 
     * 模型的输出张量在allTensors数组中的下标。
     */
    unsigned int[] outputIndex;
    /** 
     * 模型中所有的算子节点组成的数组，详情请参考：{@link Node}。
     */
    struct Node[] nodes;
    /** 
     * 模型中所有的张量组成的数组，该数组中包括输入张量，输出张量和常量张量，详情请参考：{@link Tensor}。
     */
    struct Tensor[] allTensors;
    /** 
     * 模型中所有的子图组成的数组，详情请参考：{@link SubGraph}。
     */
    struct SubGraph[] subGraph;
};

/** @} */