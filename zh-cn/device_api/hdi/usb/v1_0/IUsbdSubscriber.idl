/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup USB
 * @{
 *
 * @brief 提供统一的USB驱动标准接口，实现USB驱动接入。
 *
 * 上层USB服务开发人员可以根据USB驱动模块提供的标准接口获取如下功能：打开/关闭设备，获取设备描述符，获取文件描述符，打开/关闭接口，批量读取/写入数据，
 * 设置/获取设备功能，绑定/解绑订阅者等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IUsbdSubscriber.idl
 *
 * @brief USB驱动的订阅函数。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief USB驱动接口的包路径。
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.usb.v1_0;

import ohos.hdi.usb.v1_0.UsbTypes;

/**
 * @brief 定义USB驱动的订阅类相关函数。
 *
 * 当设备接入/断开会调用DeviceEvent进行信息上报。
 * 当端口状态发生变化时会调用PortChangedEvent进行信息上报。
 */
[callback] interface IUsbdSubscriber {
    /**
     * @brief 设备状态改变事件。
     *
     * @param UsbInfo USB设备信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    DeviceEvent([in] struct USBDeviceInfo info);

    /**
     * @brief 端口改变事件。
     *
     * @param info 端口信息。
     *
     * @return 0 表示操作成功。
     * @return 非零值 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    PortChangedEvent([in] struct PortInfo info);
}
/** @} */
