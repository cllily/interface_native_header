/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiHci
 * @{
 *
 * @brief HdiHci为HCI服务提供统一接口。
 *
 * 主机可以使用该模块提供的接口来初始化HCI（主机控制器接口），并通过该服务与控制器交换数据。
 *
 * @since 3.2
 */

/**
 * @file IHciInterface.idl
 *
 * @brief 声明接口以初始化HCI，向控制器发送数据及关闭HCI接口。
 *
 * 模块包路径：ohos.hdi.bluetooth.hci.v1_0
 *
 * 引用：
 * - ohos.hdi.bluetooth.hci.v1_0.IHciCallback
 * - ohos.hdi.bluetooth.hci.v1_0.HciTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.bluetooth.hci.v1_0;

import ohos.hdi.bluetooth.hci.v1_0.IHciCallback;
import ohos.hdi.bluetooth.hci.v1_0.HciTypes;

/**
 * @brief 声明接口以初始化HCI，向控制器发送数据及关闭HCI接口。
 *
 * @since 3.2
 */
interface IHciInterface {
    /**
     * @brief 初始化HCI并注册回调函数。
     *
     * @param callbackObj 声明回调函数。相关详细信息，请参考{@link IHciCallback}.
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    Init([in] IHciCallback callbackObj);

    /**
     * @brief 向控制器发送数据包。
     *
     * @param type 声明HCI数据包类型。相关详细信息，请参考{@link BtType}.
     * @param data 表示发送到控制器的HCI数据包。
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    SendHciPacket([in] enum BtType type, [in] unsigned char[] data);

    /**
     * @brief 关闭HCI接口。
     *
     * @return 如果操作成功返回0；否则返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    Close();
}
/** @} */