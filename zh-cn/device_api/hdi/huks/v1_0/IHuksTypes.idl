/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfHuks
 * @{
 *
 * @brief 提供通用密钥库服务（OpenHarmony Universal KeyStore，简称HUKS）核心逻辑的标准驱动API接口。
 *
 * HUKS驱动API定义了HUKS核心组件（HUKS Core）的标准接口，向HUKS服务侧提供了统一的驱动接口，
 * 这些接口涵盖了密钥全生命周期管理，包括密钥生成、密钥导入导出、密钥操作、 密钥访问控制、密钥证明等功能。
 *
 * @since 4.0
 */

 /**
 * @file IHuksTypes.idl
 *
 * @brief 定义了HUKS的驱动接口的结构体。
 *
 * 模块包路径：ohos.hdi.huks.v1_0
 *
 * @since 4.0
 */

package ohos.hdi.huks.v1_0;

/**
 * @brief HUKS的二进制数据结构体，用于封装密钥材料和密钥属性等参数。
 *
 * @since 4.0
 * @version 1.0
 */
struct HuksBlob {
    /**
     * 二进制数据
     */
    unsigned char[] data;
};

/**
 * @brief 定义了HUKS的密钥参数集数据结构体。
 *
 * @since 4.0
 * @version 1.0
 */
struct HuksParamSet {
    /**
     * 参数集序列化后的数据，属性集结构参见《HUKS设备开发指南》。
     */
    unsigned char[] data;
};

/**
 * @brief 芯片平台密钥解密场景枚举。
 *
 * @since 4.0
 * @version 1.0
 */
enum HuksChipsetPlatformDecryptScene {
    /**
     * 针对TEE（Trusted Execution Environment）环境中的应用（Trusted Application)开放的平台密钥解密场景。
     */
    HUKS_CHIPSET_PLATFORM_DECRYPT_SCENCE_TA_TO_TA = 1,
};
/** @} */