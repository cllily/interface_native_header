/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 *
 * @since 10
 */

 /**
 * @file condition_variable.h
 *
 * @brief 声明条件变量提供的C接口.
 *
 * @since 10
 */
#ifndef FFRT_API_C_CONDITION_VARIABLE_H
#define FFRT_API_C_CONDITION_VARIABLE_H
#include <time.h>
#include "type_def.h"

/**
 * @brief 初始化条件变量.
 *
 * @param cond 条件变量指针.
 * @param attr 条件变量属性指针.
 * @return 初始化条件变量成功返回ffrt_thrd_success,
           初始化条件变量失败返回ffrt_thrd_error.
 * @since 10
 */
FFRT_C_API int ffrt_cond_init(ffrt_cond_t* cond, const ffrt_condattr_t* attr);

/**
 * @brief 唤醒阻塞在条件变量上的一个任务.
 *
 * @param cond 条件变量指针.
 * @return 唤醒成功返回ffrt_thrd_success,
           唤醒失败返回ffrt_thrd_error.
 * @since 10
 */
FFRT_C_API int ffrt_cond_signal(ffrt_cond_t* cond);

/**
 * @brief 唤醒阻塞在条件变量上的所有任务.
 *
 * @param cond 条件变量指针.
 * @return 唤醒成功返回ffrt_thrd_success,
           唤醒失败返回ffrt_thrd_error.
 * @since 10
 */
FFRT_C_API int ffrt_cond_broadcast(ffrt_cond_t* cond);

/**
 * @brief 条件变量等待函数，条件变量不满足时阻塞当前任务.
 *
 * @param cond 条件变量指针.
 * @param mutex mutex指针.
 * @return 等待后被成功唤醒返回ffrt_thrd_success,
           等待失败返回ffrt_thrd_error.
 * @since 10
 */
FFRT_C_API int ffrt_cond_wait(ffrt_cond_t* cond, ffrt_mutex_t* mutex);

/**
 * @brief 条件变量超时等待函数，条件变量不满足时阻塞当前任务，超时等待返回.
 *
 * @param cond 条件变量指针.
 * @param mutex mutex指针.
 * @param time_point 最大等待到的时间点，超过这个时间点等待返回.
 * @return 等待后被成功唤醒返回ffrt_thrd_success,
           等待超时返回ffrt_thrd_timedout.
           等待失败ffrt_thrd_error.
 * @since 10
 */
FFRT_C_API int ffrt_cond_timedwait(ffrt_cond_t* cond, ffrt_mutex_t* mutex, const struct timespec* time_point);

/**
 * @brief 销毁掉件变量.
 *
 * @param cond 条件变量指针.
 * @return 销毁条件变量成功返回ffrt_thrd_success,
           销毁条件变量失败返回ffrt_thrd_error.
 * @since 10
 */
FFRT_C_API int ffrt_cond_destroy(ffrt_cond_t* cond);
/** @} */
#endif
