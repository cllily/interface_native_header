/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_EventModule
 * @{
 *
 * @brief 在Native端提供ArkUI的UI输入事件能力。
 *
 * @since 12
 */

/**
 * @file ui_input_event.h
 *
 * @brief 提供ArkUI在Native侧的事件定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef UI_INPUT_EVENT
#define UI_INPUT_EVENT

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief UI输入事件定义。
 *
 * @since 12
 */
typedef struct ArkUI_UIInputEvent ArkUI_UIInputEvent;

/**
 * @brief UI输入事件类型定义。
 *
 * @since 12
 */
typedef enum {
    ARKUI_UIINPUTEVENT_TYPE_UNKNOWN = 0,
    ARKUI_UIINPUTEVENT_TYPE_TOUCH = 1,
    ARKUI_UIINPUTEVENT_TYPE_AXIS = 2,
} ArkUI_UIInputEvent_Type;

/**
 * @brief 定义输入事件的Action Code。
 *
 * @since 12
 */
enum {
    /** 触摸取消。 */
    UI_TOUCH_EVENT_ACTION_CANCEL = 0,
    /** 触摸按下。 */
    UI_TOUCH_EVENT_ACTION_DOWN = 1,
    /** 触摸移动。 */
    UI_TOUCH_EVENT_ACTION_MOVE = 2,
    /** 触摸抬起。 */
    UI_TOUCH_EVENT_ACTION_UP = 3,
};

/**
 * @brief 产生输入事件的工具类型定义。
 *
 * @since 12
 */
enum {
    /** 不支持的工具类型。 */
    UI_INPUT_EVENT_TOOL_TYPE_UNKNOWN = 0,

    /** 手指。 */
    UI_INPUT_EVENT_TOOL_TYPE_FINGER = 1,

    /** 笔。 */
    UI_INPUT_EVENT_TOOL_TYPE_PEN = 2,

    /** 鼠标。 */
    UI_INPUT_EVENT_TOOL_TYPE_MOUSE = 3,

    /** 触控板。 */
    UI_INPUT_EVENT_TOOL_TYPE_TOUCHPAD = 4,

    /** 操纵杆。 */
    UI_INPUT_EVENT_TOOL_TYPE_JOYSTICK = 5,
};

/**
 * @brief 产生输入事件的来源类型定义。
 *
 * @since 12
 */
enum {
    /** 不支持的来源类型。 */
    UI_INPUT_EVENT_SOURCE_TYPE_UNKNOWN = 0,
    /** 鼠标。 */
    UI_INPUT_EVENTT_SOURCE_TYPE_MOUSE = 1,
    /** 触摸屏。 */
    UI_INPUT_EVENTT_SOURCE_TYPE_TOUCH_SCREEN = 2,
};

/**
 * @brief 定义触摸测试类型的枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 默认触摸测试效果，自身和子节点都响应触摸测试，但会阻塞兄弟节点的触摸测试。 */
    HTMDEFAULT = 0,

    /** 自身响应触摸测试，阻塞子节点和兄弟节点的触摸测试。 */
    HTMBLOCK,

    /** 自身和子节点都响应触摸测试，不会阻塞兄弟节点的触摸测试。 */
    HTMTRANSPARENT,

    /** 自身不响应触摸测试，不会阻塞子节点和兄弟节点的触摸测试。 */
    HTMNONE,
} HitTestMode;

/**
 * @brief 获取UI输入事件的类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前UI输入事件的类型，如果参数异常则返回0。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取UI输入事件的操作类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前UI输入事件的操作类型，如果参数异常则返回0。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetAction(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取产生UI输入事件的来源类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回产生当前UI输入事件的来源类型。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetSourceType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取产生UI输入事件的工具类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回产生当前UI输入事件的工具类型。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetToolType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取UI输入事件发生的时间。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回UI输入事件发生的时间，如果参数异常则返回0。
 * @since 12
 */
int64_t OH_ArkUI_UIInputEvent_GetEventTime(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取多点触控的接触点数量。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件的接触点数量。
 * @since 12
 */
uint32_t OH_ArkUI_PointerEvent_GetPointerCount(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取多点触控的接触点标识。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回特定接触点标识。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_GetPointerId(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetX(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetXByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetYByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowX(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowXByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowYByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayX(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayXByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayYByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取触屏压力。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件产生的触屏压力，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetPressure(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取相对YZ平面的角度，取值的范围[-90, 90]，其中正值是向右倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中相对YZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTiltX(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取相对XZ平面的角度，值的范围[-90, 90]，其中正值是向下倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中相对XZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTiltY(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取触屏区域的宽度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的宽度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTouchAreaWidth(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取触屏区域的高度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的高度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTouchAreaHeight(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取历史事件数量。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前历史事件数量。
 * @since 12
 */
uint32_t OH_ArkUI_PointerEvent_GetHistorySize(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取历史事件发生的时间。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回UI输入事件发生的时间，如果参数异常则返回0。
 * @since 12
 */
int64_t OH_ArkUI_PointerEvent_GetHistoryEventTime(const ArkUI_UIInputEvent* event, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中多点触控的接触点数量。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 特定历史事件中多点触控的接触点数量。
 * @since 12
 */
uint32_t OH_ArkUI_PointerEvent_GetHistoryPointerCount(const ArkUI_UIInputEvent* event, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取多点触控的接触点标识。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回特定历史事件中的特定接触点标识。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_GetHistoryPointerId(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryX(const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryY(const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryWindowX(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryWindowY(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryDisplayX(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryDisplayY(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的触屏压力。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件产生的触屏压力，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryPressure(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的相对YZ平面的角度，取值的范围[-90, 90]，其中正值是向右倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中相对YZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTiltX(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的相对XZ平面的角度，值的范围[-90, 90]，其中正值是向下倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中相对XZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTiltY(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的触屏区域的宽度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的宽度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTouchAreaWidth(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的触屏区域的高度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的高度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTouchAreaHeight(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 获取当前轴事件的垂直滚动轴的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的垂直滚动轴的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetVerticalAxisValue(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前轴事件的水平滚动轴的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的水平滚动轴的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetHorizontalAxisValue(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前轴事件的捏合轴缩放的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的捏合轴缩放的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetPinchAxisScaleValue(const ArkUI_UIInputEvent* event);

/**
 * @brief 配置HitTest模式。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param mode 指定HitTest模式，参数类型{@link HitTestMode}。
 * @return 返回执行的状态代码。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_SetInterceptHitTestMode(const ArkUI_UIInputEvent* event, HitTestMode mode);

#ifdef __cplusplus
};
#endif

#endif // UI_INPUT_EVENT
/** @} */
